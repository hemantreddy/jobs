This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Assumptions

1. The user is going to enter only of the three fields. although the code I've written will work to a certain extant, my main goal was former of the two. 
2. No restriction on the use of styling libraries.

### Notes

1. Please note that I had only a couple of hours to work on this, I saw the email only yesterday. I can definitely improve it if I can get a little more time. 
2. To continue on the previous point, I would concentrate on testing and UI improvement. 

### How to use
1. This project is deployed on [heroku](https://intense-wildwood-39971.herokuapp.com/)
2. I have implemented pagination, so look for the number tiles at the end of the page. 
3. The skills field need to be provided with comma separated values for intended results.

