import React from 'react'

const Pagination = ({ nextPage, currentPage, totalResults, postsPerPage }) => {
  const pageLinks = [];
  for (let i = 1; i <= Math.floor(totalResults.length / postsPerPage); i++) {
    pageLinks.push(i);
  }
  return (
    <div className="container">
      <div className="row">
        <div className="pagination">
          {
            pageLinks.map(number => (
              <li className={`waves-effect ${currentPage === number ? 'active' : null}`} key={number} onClick={() => nextPage(number)}>
                <a href="!#">
                  {number}
                </a>
              </li>
            ))
          }
        </div>
      </div>
    </div>
  )
}

export default Pagination;
