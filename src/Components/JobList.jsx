import React from 'react';
import Job from './Job';

const JobList = ({ loading, jobs, modalIsOpen, toggleModal }) => {
  if (loading) {
    return (
      <h2>..loading</h2>
    )
  }
  return (
    <div className="row">
      <div className="col s12">
        {
          jobs.map(job => {
            return (
              <Job
                key={job._id}
                title={job.title}
                location={job.location}
                skills={job.skills}
                experience={job.experience}
                companyname={job.companyname}
                applylink={job.applylink}
                modalIsOpen={modalIsOpen}
                toggleModal={toggleModal}
              />
            )
          })
        }
      </div>
    </div>
  )
}

export default JobList;
