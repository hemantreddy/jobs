import React from 'react';

const Job = ({ title, location, experience, skills, applylink, companyname }) => {
  return (
    <div className="col s12 m6 l3">
      <div className="card small blue-grey darken-1">
        <div className="card-content white-text">
          <span className="card-title">{companyname}</span>
          <p>{title}</p>
          {skills ? <p><strong>Skills Required:</strong> {skills.split('').slice(0, 30)}</p> : null}
          {experience ? <p><strong>Experience:</strong> {experience}</p> : null}
          {location ? <p><strong>Location:</strong> {location.split(',').slice(0, 1)}</p> : null}
        </div>
        <div className="card-action">
          <a href={applylink}>Apply</a>
        </div>
      </div>
    </div>
  )
}

export default Job;
