import React from 'react';
import { Container, Button, Alert, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

function MyVerticallyCenteredModal({ modalIsOpen, toggleModal }) {
  return (
    <Container>
      {/* <button color="primary" onClick={toggleModal}> Open Modal </button> */}
      <Modal isOpen={modalIsOpen}>
        <ModalHeader toggle={toggleModal}>Model Title</ModalHeader>
        <ModalBody>Model Title</ModalBody>
        <ModalFooter>
          <Button color="primary" >Sign up</Button>
          <Button color="secondary" >Close</Button>
        </ModalFooter>
      </Modal>
    </Container>
  );
}

export default MyVerticallyCenteredModal;