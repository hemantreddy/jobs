import React from 'react'

const SearchArea = ({ onChange, handleSearch }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col s12 m4 l2">
          <div className="input-field">
            <input
              placeholder="Location"
              type="text"
              name="location"
              onChange={onChange}
            />
          </div>
        </div>
        <div className="col s12 m4 l8">
          <div className="input-field">
            <input
              placeholder="Skills"
              name="skills"
              type="text"
              onChange={onChange}
            />
          </div>
        </div>
        <div className="col s12 m4 l2">
          <div className="input-field">
            <input
              placeholder="Experience"
              name="experience"
              type="text"
              onChange={onChange}
            />
          </div>
        </div>
      </div>
      <button
        className="btn waves-effect waves-light"
        type="submit"
        onClick={handleSearch}
        name="submit">Submit
      </button>
    </div>
  )
}

export default SearchArea;
