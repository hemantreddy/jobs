import React from 'react';
import './App.css';

// Components
import Nav from './Components/Nav'
import SearchArea from './Components/SearchArea';
import JobList from './Components/JobList';
import Pagination from './Components/Pagination'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      jobs: [],
      searchValue: '',
      loading: false,
      totalResults: 0,
      currentPage: 1,
      postsPerPage: 20
    }
  }

  componentDidMount() {
    let url = 'https://cors-anywhere.herokuapp.com/https://nut-case.s3.amazonaws.com/jobs.json';
    fetch(url)
      .then(res => res.json())
      .then(job => this.setState({ totalJobs: job.data.length }))
      .catch(err => console.log('no data received'))
  }

  onChange = (event) => {
    const { name, value } = event.target;
    if (name === 'skills') {
      let skills = value.split(",")
      this.setState({ skills: skills })
    } else {
      this.setState({ [name]: value })
    }
  }

  makeApiCall = () => {
    this.setState({ loading: true, jobs: [] })
    let url = 'https://cors-anywhere.herokuapp.com/https://nut-case.s3.amazonaws.com/jobs.json';
    fetch(url)
      .then(res => res.json())
      .then(jobs => {
        if (this.state.location) {
          let regex = new RegExp(this.state.location, "gi")
          jobs.data.forEach(job => {
            if (regex.test(job.location)) {
              this.setState({ jobs: [...this.state.jobs, job] })
            }
          })
        } else if (this.state.skills) {
          const { skills } = this.state;
          let i = 0;
          while (i < skills.length) {
            let regex = new RegExp(skills[i], "gi")
            jobs.data.forEach(job => {
              if (regex.test(job.skills)) {
                this.setState({ jobs: [...this.state.jobs, job] })
              }
            })
            i++;
          }
        } else if (this.state.experience) {
          let userExperience = parseInt(this.state.experience);
          jobs.data.forEach(async job => {
            if (job.experience && job.experience !== "") {
              console.log(job.experience)
              // let experience = job.experience;
              let exp = [];
              let min = parseInt(job.experience[0])
              let max = parseInt(job.experience[2])
              for (let i = min; i <= max; i++) {
                exp.push(i)
              }
              // console.log(userExperience)
              if (exp.includes(userExperience)) {
                console.log('inside if')
                this.setState({ jobs: [...this.state.jobs, job] })
              }
            }
          })
        }
      })
      .then(() => {
        console.log(this.state.jobs)
        this.setState(
          { loading: false }
        )
      })
      .then(() => this.setState({ totalResults: this.state.jobs }))
      .catch(err => console.log(err))
  }

  handleSearch = () => {
    this.makeApiCall(this.state.searchValue);
  }

  nextPage = (pageNumber) => {
    this.setState({ currentPage: pageNumber })
  }

  render() {

    // const totalPages = Math.floor(this.state.totalResults / this.state.postsPerPage);
    const indexOfLastPost = this.state.currentPage * this.state.postsPerPage;
    const indexOfFirstPost = indexOfLastPost - this.state.postsPerPage;
    const currentPosts = this.state.jobs.slice(indexOfFirstPost, indexOfLastPost);

    return (
      <div className="App">
        <Nav />
        <SearchArea
          onChange={this.onChange}
          handleSearch={this.handleSearch}
        />
        {this.state.totalResults ? <h4>Your search returned {this.state.totalResults.length} jobs</h4> : <h4>Search out of {this.state.totalJobs} jobs on our site</h4>}
        <JobList
          jobs={currentPosts}
          loading={this.state.loading}
        />
        <Pagination
          // pages={totalPages}
          nextPage={this.nextPage}
          currentPage={this.state.currentPage}
          postsPerPage={this.state.postsPerPage}
          totalResults={this.state.totalResults}
        />
      </div>
    )
  }
}

export default App;
